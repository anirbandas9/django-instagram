from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from .models import Post
from relationships.models import Relationship
from .forms import PostForm
from .utils import delete_old_image
from django.contrib.auth.models import User
from comments.models import Comment

from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import HttpResponseForbidden

# from django.db.models import Q
# from django.core.paginator import Paginator
# from relationships.models import Relationship
# from django.contrib.auth.models import User


# @login_required
# def post_feed(request):
#     user = request.user
#     following_users = Relationship.objects.filter(follower=user).values_list('following', flat=True)
    
#     post_items = Post.objects.filter(user__in=following_users).order_by('-created_at')

#     # Pagination
#     paginator = Paginator(post_items, 6)
#     page_number = request.GET.get('page')
#     post_items_paginator = paginator.get_page(page_number)
    
#     # Search users
#     query = request.GET.get('q')
#     users_paginator = None  # Initialize users_paginator as None
#     if query:
#         users = User.objects.filter(Q(username__icontains=query))

#         paginator = Paginator(users, 6)
#         page_number = request.GET.get('page')
#         users_paginator = paginator.get_page(page_number)

#     context = {
#         'post_items': post_items_paginator,
#         'users_paginator': users_paginator,
#     }
#     return render(request, 'posts/post_feed.html', context)


@login_required
def create_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()
            return redirect('posts:detail', post.pk)
    else:
        form = PostForm()
    return render(request, 'posts/create.html', {'form': form})


@login_required
def detail_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    comments = Comment.objects.filter(post=post).order_by('-created_at')
    return render(request, 'posts/detail.html', {'post': post, 'comments': comments})


@login_required
def update_post(request, pk):
    post = get_object_or_404(Post, pk=pk)

    if request.user != post.user:
        return redirect('posts:detail', pk)

    if request.method == 'POST':
        old_image_path = post.image.path
        form = PostForm(request.POST, request.FILES, instance=post)
        if form.is_valid():
            if 'image' in request.FILES:
                delete_old_image(old_image_path)
            form.save()
            return redirect('posts:detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'posts/update.html', {'form': form})


@login_required
def delete_post(request, pk):
    post = get_object_or_404(Post, pk=pk)

    if request.user != post.user:
        return redirect('posts:detail', pk)

    if request.method == 'POST':
        old_image_path = post.image.path
        post.delete()
        delete_old_image(old_image_path)
        return redirect('posts:list')

    return render(request, 'posts/confirm_delete.html', {'post': post})


@login_required
def list_posts(request):
    following_users = Relationship.objects.filter(follower=request.user).values_list('following')
    posts = Post.objects.filter(user__in=following_users).order_by('-created_at')
    users = User.objects.all()
    return render(request, 'posts/list.html', {'posts': posts, 'users': users})


@login_required
def like_post(request, pk):
    post = get_object_or_404(Post, pk=pk)

    if request.user.is_authenticated:
        if request.user in post.likes.all():
            post.likes.remove(request.user)
        else:
            post.likes.add(request.user)

    return HttpResponseRedirect(reverse('posts:detail', args=[pk]))


# Create your views here.
# def create_comment(request, post_id):
#     post = get_object_or_404(Post, id=post_id)
#     if request.method == 'POST':
#         form = CommentForm(request.POST)
#         if form.is_valid():
#             comment = form.save(commit=False)
#             comment.post = post
#             comment.user = request.user
#             comment.save()
#             return redirect('posts:detail', pk=post.id)
#     else:
#         form = CommentForm()
#     return render(request, 'posts/create_comment.html', {'form': form})


# def update_comment(request, comment_id):
#     comment = get_object_or_404(Comment, id=comment_id)
#     if request.user != comment.user:
#         return HttpResponseForbidden('You cannot edit this comment.')

#     if request.method == 'POST':
#         form = CommentForm(request.POST, instance=comment)
#         if form.is_valid():
#             form.save()
#             return redirect('posts:detail', pk=comment.post.id)
#     else:
#         form = CommentForm(instance=comment)
#     return render(request, 'posts/update_comment.html', {'form': form})


# def delete_comment(request, comment_id):
#     comment = get_object_or_404(Comment, id=comment_id)
#     if request.user != comment.user and request.user != comment.post.user:
#         return HttpResponseForbidden('You cannot delete this comment.')

#     if request.method == 'POST':
#         post_id = comment.post.id
#         comment.delete()
#         return redirect('posts:detail', pk=post_id)
#     return render(request, 'posts/delete_comment.html', {'comment': comment})
