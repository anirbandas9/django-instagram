from django.urls import path
from . import views

app_name = 'posts'

urlpatterns = [
    path('posts/create/', views.create_post, name='create'),
    path('posts/detail/<int:pk>/', views.detail_post, name='detail'),
    path('posts/update/<int:pk>/', views.update_post, name='update'),
    path('posts/delete/<int:pk>/', views.delete_post, name='delete'),
    path('', views.list_posts, name='list'),
    path('posts/like/<int:pk>/', views.like_post, name='like'),
    # path('create_comment/<int:post_id>/', views.create_comment, name='create_comment'),
    # path('update_comment/<int:comment_id>/', views.update_comment, name='update_comment'),
    # path('delete_comment/<int:comment_id>/', views.delete_comment, name='delete_comment'),
    # path('post_feed/', views.post_feed, name='post_feed'),
]
