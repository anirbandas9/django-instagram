from django import forms
from .models import Post


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['image', 'caption']


# class CommentForm(forms.ModelForm):
#     class Meta:
#         model = Comment
#         fields = ('text', 'parent')
#         widgets = {
#             'text': forms.Textarea(attrs={'rows': 3}),
#             'parent': forms.HiddenInput(),
#         }
