import os
from django.conf import settings


def delete_old_image(old_image_path):
    old_image_full_path = os.path.join(settings.MEDIA_ROOT, old_image_path)

    if os.path.exists(old_image_full_path):
        os.remove(old_image_full_path)
