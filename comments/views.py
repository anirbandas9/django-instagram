from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from posts.models import Post
from .models import Comment
from django.http import HttpResponseForbidden
from .forms import CommentForm


# Create your views here.
@login_required
def create_comment(request, post_id):
    post = get_object_or_404(Post, id=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.user = request.user
            comment.save()
            return redirect('posts:detail', pk=post.id)
    else:
        form = CommentForm()
    return render(request, 'comments/create_comment.html', {'form': form})


@login_required
def update_comment(request, comment_id):
    comment = get_object_or_404(Comment, id=comment_id)
    if request.user != comment.user:
        return HttpResponseForbidden('You cannot edit this comment.')

    if request.method == 'POST':
        form = CommentForm(request.POST, instance=comment)
        if form.is_valid():
            form.save()
            return redirect('posts:detail', pk=comment.post.id)
    else:
        form = CommentForm(instance=comment)
    return render(request, 'comments/update_comment.html', {'form': form})


@login_required
def delete_comment(request, comment_id):
    comment = get_object_or_404(Comment, id=comment_id)
    if request.user != comment.user and request.user != comment.post.user:
        return HttpResponseForbidden('You cannot delete this comment.')

    if request.method == 'POST':
        post_id = comment.post.id
        comment.delete()
        return redirect('posts:detail', pk=post_id)
    return render(request, 'comments/delete_comment.html', {'comment': comment})
