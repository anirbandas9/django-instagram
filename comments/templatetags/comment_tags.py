from django import template

register = template.Library()


@register.inclusion_tag('comments/comment_partial.html')
def render_nested_comments(comments, level=0):
    return {'comments': comments, 'level': level}


@register.simple_tag
def root_comments(post, *args, **kwargs):
    return post.comment_set.filter(parent=None)
