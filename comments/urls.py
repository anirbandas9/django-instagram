from django.urls import path
from . import views

app_name = 'comments'

urlpatterns = [
    path('create/<int:post_id>/', views.create_comment, name='create_comment'),
    path('update/<int:comment_id>/', views.update_comment, name='update_comment'),
    path('delete/<int:comment_id>/', views.delete_comment, name='delete_comment'),
]
