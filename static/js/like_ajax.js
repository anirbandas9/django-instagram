$(document).ready(function () {
    $(".like-btn").click(function (event) {
        event.preventDefault();
        const csrfToken = $("input[name='csrfmiddlewaretoken']").val();
        const postId = $(this).data("post-id");
        const likeUrl = `/posts/ajax_like/${postId}/`;

        $.ajax({
            url: likeUrl,
            type: "POST",
            headers: {
                "X-CSRFToken": csrfToken,
            },
            success: function (data) {
                if (data.liked) {
                    $(`#like-btn-${postId}`).text("Unlike");
                } else {
                    $(`#like-btn-${postId}`).text("Like");
                }
                $(`#likes-count-${postId}`).text(`${data.likes_count} Likes`);
            },
        });
    });
});
