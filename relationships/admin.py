from django.contrib import admin

# Register your models here.

from .models import Relationship

admin.site.register(Relationship)
