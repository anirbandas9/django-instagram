# relationships/views.py

from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import Relationship


@login_required
def follow(request, pk):
    user_to_follow = get_object_or_404(User, pk=pk)
    Relationship.objects.create(follower=request.user, following=user_to_follow)
    return redirect('profiles:detail', pk=user_to_follow.profile.pk)


@login_required
def unfollow(request, pk):
    user_to_unfollow = get_object_or_404(User, pk=pk)
    relationship = get_object_or_404(Relationship, follower=request.user, following=user_to_unfollow)
    relationship.delete()
    return redirect('profiles:detail', pk=user_to_unfollow.profile.pk)
