# relationships/urls.py

from django.urls import path
from . import views

app_name = 'relationships'

urlpatterns = [
    path('follow/<int:pk>/', views.follow, name='follow'),
    path('unfollow/<int:pk>/', views.unfollow, name='unfollow'),
]
