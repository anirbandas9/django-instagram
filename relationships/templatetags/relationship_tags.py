from django import template

register = template.Library()


@register.filter(name='is_following')
def is_following(user, target_user):
    return user.following.filter(following=target_user).exists()
