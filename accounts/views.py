from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .forms import SignUpForm, LoginForm
from relationships.models import Relationship
from posts.models import Post
from django.contrib.auth.models import User


def signup(request):
    if request.user.is_authenticated:
        return redirect('welcome')

    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('profiles:create')
    else:
        form = SignUpForm()
    return render(request, 'accounts/signup.html', {'form': form})


def user_login(request):
    if request.user.is_authenticated:
        return redirect('welcome')

    if request.method == 'POST':
        form = LoginForm(request, request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('welcome')
    else:
        form = LoginForm()
    return render(request, 'accounts/login.html', {'form': form})


def user_logout(request):
    logout(request)
    return redirect('login')


@login_required
def welcome(request):
    # following_users = Relationship.objects.filter(follower=request.user).values_list('following')
    # posts = Post.objects.filter(user__in=following_users).order_by('-created_at')
    following_users = Relationship.objects.filter(follower=request.user).values_list('following')
    posts = Post.objects.filter(user__in=following_users).order_by('-created_at')
    users = User.objects.all()
    context = {
        'posts': posts,
        'users': users,
    }

    return render(request, 'accounts/welcome.html', context)
