from django.urls import path
from . import views

app_name = 'profiles'

urlpatterns = [
    path('create/', views.create_profile, name='create'),
    path('detail/<int:pk>/', views.detail_profile, name='detail'),
    path('update/<int:pk>/', views.update_profile, name='update'),
    # path('delete/<int:pk>/', views.delete_profile, name='delete'),
    path('other_users/', views.other_users, name='other_users'),
]
