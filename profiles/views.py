from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
# from django.http import HttpResponseForbidden
# from django.http import HttpResponseRedirect
from .models import Profile
from .forms import ProfileForm
from posts.models import Post
from relationships.models import Relationship
from django.db.models import Count


@login_required
def create_profile(request):
    if hasattr(request.user, 'profile'):
        return redirect('profiles:detail', request.user.profile.pk)

    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            profile = form.save(commit=False)
            profile.user = request.user
            profile.save()

            request.user.first_name = form.cleaned_data['first_name']
            request.user.last_name = form.cleaned_data['last_name']
            request.user.save()

            return redirect('profiles:detail', profile.pk)
    else:
        form = ProfileForm()
    return render(request, 'profiles/create.html', {'form': form})


@login_required
def detail_profile(request, pk):
    profile = get_object_or_404(Profile, pk=pk)
    posts = Post.objects.filter(user=profile.user).annotate(comment_count=Count('comment')).order_by('-created_at')
    following_count = Relationship.objects.filter(follower=profile.user).count()
    followers_count = Relationship.objects.filter(following=profile.user).count()
    posts_count = len(posts)
    context = {
        'profile': profile,
        'posts': posts,
        'following_count': following_count,
        'followers_count': followers_count,
        'posts_count': posts_count,
    }
    return render(request, 'profiles/detail.html', context)


# @login_required
# def update_profile(request, pk):
#     profile = get_object_or_404(Profile, pk=pk)
#     next_page = request.GET.get('next', '/')

#     if request.user != profile.user:
#         return HttpResponseRedirect(next_page)

#     if request.method == 'POST':
#         form = ProfileForm(request.POST, request.FILES, instance=profile)
#         if form.is_valid():
#             form.save()
#             return redirect('profiles:detail', pk)
#     else:
#         form = ProfileForm(instance=profile)
#     return render(request, 'profiles/update.html', {'form': form})


@login_required
def update_profile(request, pk):
    profile = get_object_or_404(Profile, pk=pk)

    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=profile, user=request.user)
        if form.is_valid():
            form.save()

            request.user.first_name = form.cleaned_data['first_name']
            request.user.last_name = form.cleaned_data['last_name']
            request.user.save()
            return redirect('profiles:detail', pk)
    else:
        form = ProfileForm(instance=profile, user=request.user)
    return render(request, 'profiles/update.html', {'form': form})


# @login_required
# def delete_profile(request, pk):
#     profile = get_object_or_404(Profile, pk=pk)
#     if request.method == 'POST':
#         profile.delete()
#         return redirect('accounts:welcome')
#     return render(request, 'profiles/delete.html', {'profile': profile})


@login_required
def other_users(request):
    users = User.objects.exclude(pk=request.user.pk)  # Excluding the current user
    context = {
        'users': users,
    }
    return render(request, 'profiles/other_users.html', context)
