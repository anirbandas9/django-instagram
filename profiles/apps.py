from django.apps import AppConfig


class ProfilesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'profiles'


class RelationshipsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'relationships'


default_app_config = 'relationships.RelationshipsConfig'
